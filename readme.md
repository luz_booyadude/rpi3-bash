<h1>Raspberry Pi 3 Shell Scripts</h1>

This repo is collection of shell script that enhance your RPi3.

<h2>List of scripts</h2>
<ol>
<li><b>sd2hdd.sh</b>
  <br/>
  Run the system entirely on HDD. Due to reliability issue of booting from USB, SD card still needed to boot the system. After that it will hand it back to HDD. This dramatically improve the SD card life time.<br/>
  <code>sudo chmod +x sd2hdd.sh</code>
  <br/>
  <code>./sd2hdd.sh</code>
</li>
</ol>

<h2>Installation</h2>

<ol>
<li>Clone <a href="https://gitlab.com/luz_booyadude/rpi3-bash">RPi3 Shell Scripts</a> into your user's home folder (usually /home/pi):
  <br/>
  <code>git clone https://gitlab.com/luz_booyadude/rpi3-bash</code>
</li>
<li>Execute the installer:
  <br/>
  <code>sudo chmod +x <i>your_desired_scripts.sh</i></code>
  <br/>
  <code>./<i>your_desired_scripts.sh</i></code>
</li>
</li>
<li>Reboot the Pi, it should work!</li>
</ol>