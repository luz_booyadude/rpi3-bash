#!/bin/bash
#
# Installer for moving system data from SD card to USB HDD
#
# Usage:
# 	chmod +x install.sh
#	./install.sh
#

# Install package
sudo apt update
sudo apt install nginx php7.0 shellinabox aria2 git python python3 minidlna

# Create www folder in home directory
mkdir ~/www
mkdir ~/www/aria2

# Download aria2 web ui from https://github.com/mayswind/AriaNg/releases
wget https://github.com/mayswind/AriaNg/releases/download/0.4.0/aria-ng-0.4.0.zip
mkdir ~/temp
unzip aria-ng-0.4.0.zip -d ~/temp
mv ~/temp ~/www/aria2/

# Download letsencrypt
git clone https://github.com/certbot/certbot.git ~/
./home/pi/letsencrypt/certbot-auto

# Copy NGINX settings

# Copy aria2 settings

# Setup Minidlna

# Create startup script for NGINX and aria2