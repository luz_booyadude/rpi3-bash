#!/bin/bash
#
# Installer for moving system data from SD card to USB HDD
#
# Usage:
# 	chmod +x install.sh
#	./install.sh
#

RED='\033[0;31m'
GRN='\033[0;32m'
YLW='\033[1;33m'
LBL='\033[1;34m'
NC='\033[0m' # No Color

printf "${YLW}Before starting the migration, make sure${NC} ${RED}ONLY TARGET HDD${NC} ${YLW}is connected to RPi.${NC}\n"
printf "${YLW}There is only one HDD connected. [y/n]\n${NC}"
read confirm_start
if [[ $confirm_start == y* ]]
then
    printf "${YLW}\nUpdating the system...\n${NC}"
else
    printf "${YLW}\nPlease remove any other HDD except the target HDD and rerun this program again.${NC}"
    exit
fi

# Update Packages and ensure dependencies are installed
sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get install -y screen git rsync gdisk e2fsprogs

printf "${YLW}\nConfiguring...\n${NC}"

# check for HDD existence
if [ -e /dev/sda ]
then
    echo -e "${YLW}USB drive found. Proceeding...${NC}"
else
    echo -e "${YLW}No USB drive found. Aborting...${NC}"
    exit
fi

# Create partition
echo -e "${YLW}The HDD will be partitioned to several partition. Select your partition configuration:${NC}"
echo -e "${YLW}1) 2 partitions: Boot + Home${NC}"
echo -e "${YLW}2) 3 partitions: Boot + Home + Backup partition (UNSUPPORTED YET)${NC}"
read partition
if [[ $partition == 1 ]] ;
then
	printf "${YLW}\n2 partitions will be created${NC}"
# elif [[ $partition == 2 ]]
# then
#     printf "\n3 partitions will be created"
else
    printf "${YLW}\nWrong option is selected. Aborting...${NC}"
    exit
fi

#total_partition=$(($partition + 1))

printf "${YLW}\nAll data in HDD will be DESTROYED. Proceed? [y/n]${NC}\n"
read confirm_partition
if [[ $confirm_partition == y* ]]
then
    printf "${YLW}\nRepartitioning...${NC}\n"
else
    printf "${YLW}\nAborting...${NC}"
    exit
fi

## delete all partition
sudo sgdisk --zap-all /dev/sda
## Create first partition
sudo sgdisk --new=1:0:+8G -c 1:"Boot" -t 1:8300 /dev/sda
## Create second partition
sudo sgdisk --new=2:0:0 -c 2:"User Data" -t 2:8300 /dev/sda
## Display partition info
sudo sgdisk -p /dev/sda

echo
echo

# Formatting the partitions
sudo mke2fs -t ext4 -L rootfs /dev/sda1
sudo mke2fs -t ext4 -L user_data /dev/sda2

# Create new directory for mounting
sudo mkdir /mnt/root
sudo mkdir /mnt/data

# Mount the root partition
sudo mount /dev/sda1 /mnt/root

printf "${YLW}\nCheck whether the partition is mounted correctly. Can you see the /dev/sda1 mounted to /mnt/root? [y/n]${NC}\n"
df -h|grep /dev/sda1
read mount_confirm
if [[ $mount_confirm == y* ]]
then
    printf "${YLW}\nThe partition mounted correctly. Proceeding to next step...${NC}\n"
else
    printf "${YLW}\nThe partition is not mounted. Please rerun the script${NC}"
    exit
fi

# Mount the data partition
sudo mount /dev/sda2 /mnt/data

printf "${YLW}\nCheck whether the partition is mounted correctly. Can you see the /dev/sda2 mounted to /mnt/data? [y/n]${NC}\n"
df -h|grep /dev/sda2
read mount_confirm
if [[ $mount_confirm == y* ]]
then
    printf "${YLW}\nThe partition mounted correctly. Proceeding to next step...${NC}\n"
else
    printf "${YLW}\nThe partition is not mounted. Please rerun the script${NC}"
    exit
fi

# Copying data from / to /mnt/root
printf "${YLW}\nStarting copying root...${NC}\n"
sudo rsync -axv / /mnt/root

# Copying data from /home/ to /mnt/data
printf "${YLW}\nStarting copying home...${NC}\n"
sudo rsync -axv /home/ /mnt/data

# Copying bootloader setting
## Backup cmdline.txt
sudo cp /boot/cmdline.txt /boot/cmdline.sd
## Delete old cmdline.txt
sudo rm /boot/cmdline.txt
## Copying new config
sudo cp ~/rpi3-bash/cmdline.txt /boot/

# Modify fstab to mount the new drive during boot
## Backup fstab
sudo cp /mnt/root/etc/fstab /mnt/root/etc/fstab.bak
## Delete old fstab
sudo rm /mnt/root/etc/fstab
## Copying new fstab
sudo cp ~/rpi3-bash/fstab /mnt/root/etc/

echo -e "${YLW}Done. Please reboot the system${NC}"